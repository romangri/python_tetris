##################################################
#IB910C-S VT2013
#Spel projekt
#Roman Grintsevich
#
#
#Python version: 2.7.5
#Operativ system: Windows 7
#
#Field class used to keep track on landed figures
#and keep track if any rows are filled and if
#highest row is not filled
#
##################################################

class Field(object):
    def __init__(self):
        self._columns = 16
        self._rows = 32
        self.field = [[0 for x in xrange(self._columns)] for x in xrange(self._rows)] 
        self.countFormat = 0
        self.rowsDone = 0
        self.rowCleaderFlag = False
        self.gameOver = False
        
    def resetField(self):
        for cols in range(0, self._columns):
            for raws in range(0, self._rows):
                self.field[cols][raws] = 0

    def printField(self):
        for cols in range(0, self._columns):
            for raws in range(0, self._rows):
                print self.field[cols][raws]

    def getBoard(self):
        for i in range(len(self.field)):
            for j in range(len(self.field[i])):
                print self.field[i][j],
                self.countFormat += 1
                if(self.countFormat == 16):
                    print "row", i
                    self.countFormat = 0

    def setBoard(self, ind1, ind2): #@row, @column
        self.field[ind2][ind1] = 1  #@x_cords, @y_cords
        
    def getLowestY(self, col):
        temp = 0

        for i in range(0, self._rows):
            if(self.field[i][col] == 1):
                temp = i
                break
        return temp

    def getUsedSquare(self,cols, rows):
        return self.field[rows][cols]


    def getNextY(self, col, row):
        col /= 20
        row /= 20
        tempIndex = 0
        if row < 31:
            row += 1
            tempIndex = self.field[row][col]
        elif row >= 31:
            tempIndex = 1
        return tempIndex

    def checkRows(self):
        counter = 0
        for i in range (0, 32):
            for j in range (0, 16):
                if self.field[i][j] == 1:
                    counter += 1
            if(counter == 16):
                self.clearRows(i)
                self.removeEmptyRows(i)
                self.rowsDone += 1
                self.rowCleaderFlag = True
            counter = 0
            
    def getRowsDone(self):
        return self.rowsDone

    def clearRows(self, row):
        #reset row
        #
        for i in range(0, 16):
            self.field[row][i] = 0
        #
        #reset row

    def removeEmptyRows(self, row):
        for row in range(row, 0, -1):
            for j in range(0, 16):
                self.field[row][j] = self.field[row-1][j]
                self.field[row-1][j] = 0

    def checkRowFlag(self):
        return self.rowCleaderFlag

    def resetRowFlag(self):
        self.rowCleaderFlag = False

    def gameOverFlag(self):
        for i in range(0, 16):
            if self.field[2][i] == 1:
                self.gameOver = True
                break
        return self.gameOver

    def clearAll(self):
        for i in range(0, 32):
            for j in range(0, 16):
                self.field[i][j] = 0
        self.gameOver = False
