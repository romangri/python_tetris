##################################################
#IB910C-S VT2013
#Spel projekt
#Roman Grintsevich
#
#
#Python version: 2.7.5
#Operativ system: Windows 7
#
#Position class used to get lowest and highest
#values in position array
#
##################################################

class Position(object):
    def __init__(self):
        self.leftX = 0
        self.rightX = 0
        self.upperY = 0
        self.lowerY = 0

    ###Coords X X X
    def getLeftX(self, arrayX):
        leftX = arrayX[0]
        for x in range(0, 4):
            if arrayX[x] < leftX:
                leftX = arrayX[x]
        return leftX

    def getRightX(self, arrayX):
        rightX = arrayX[0]
        for x in range(0, 4):
            if arrayX[x] > rightX:
                rightX = arrayX[x]
        return rightX
    
    ###Coords Y Y Y
    def getUpperY(self, arrayY):
        upperY = arrayY[0]
        for x in range(0, 4):
            if arrayY[x] < upperY:
                upperY = arrayY[x]
        return upperY

    def getLowerY(self, arrayY):
        lowerY = arrayY[0]
        for x in range(0, 4):
            if arrayY[x] > lowerY:
                lowerY = arrayY[x]
        return lowerY
