##################################################
#IB910C-S VT2013
#Spel projekt
#Roman Grintsevich
#
#
#Python version: 2.7.5
#Operativ system: Windows 7
#
#color class
#
##################################################

class Color(object):
    def __init__(self):
        self.white = [255,255,255]
        self.black = [0,  0,  0]
        self.red = [255, 0, 0]
        self.green = [0, 255, 0]
        self.blue = [0, 0, 255]

    def getColor(self, sCcolor):
        if(sCcolor == 'black'):
            return self.black
        if(sCcolor == 'white'):
            return self.white
        if(sCcolor == 'red'):
            return self.red
        if(sCcolor == 'green'):
            return self.green
        if(sCcolor == 'blue'):
            return self.blue
