##################################################
#IB910C-S VT2013
#Spel projekt
#Roman Grintsevich
#
#
#Python version: 2.7.5
#Operativ system: Windows 7
#
#button class
#
##################################################

import pygame
from pygame.locals import *

class Button(object):
    def __init__(self):
        self.rectColor = (255, 153, 35)
        self.thickness = 1
        self.textColor = (50, 220, 50)
        self.fontSize = 30
        self.newFont = pygame.font.SysFont("Calibri", self.fontSize)
        
    def createButton(self, surface, rectX, rectY, rectWidth, rectHeight, text):
        surface = self.drawRect(surface, self.rectColor, rectX, rectY, rectWidth, rectHeight, self.thickness)
        surface = self.writeText(surface, text, rectX, rectY, rectWidth)
        self.rect = pygame.Rect(rectX, rectY, rectWidth, rectHeight)
        return surface

    def drawRect(self, surface, color, x, y, width, height, thickness):
        pygame.draw.rect(surface, color, (x, y, width, height), thickness)
        return surface

    def writeText(self, surface, textIn, xPos, yPos, width):
        strLen = len(textIn)/2+1
        newXpos = xPos + width/2 - strLen*10
        text = self.newFont.render(textIn, 1, self.textColor)
        surface.blit(text, (newXpos, yPos+10))
        return surface

    def buttonPressed(self, mousePos):
        if mousePos[0] > self.rect.topleft[0]:
            if mousePos[1] > self.rect.topleft[1]:
                if mousePos[0] < self.rect.bottomright[0]:
                    if mousePos[1] < self.rect.bottomright[1]:
                        return True
                    else:
                        return False
                else:
                    return False
            else:
                return False
        else:
            return False
