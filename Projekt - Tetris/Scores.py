##################################################
#IB910C-S VT2013
#Spel projekt
#Roman Grintsevich
#
#
#Python version: 2.7.5
#Operativ system: Windows 7
#
#Score class used for saving scores to file
#and reading scores from file
#
##################################################

class Score(object):
    def __init__(self):
        self.tempScoreList = []
        self.gamesPlayed = 0
        self.topFiveScore = [-1, -1, -1, -1, -1]
        self.topFiveName = ['', '', '', '', '']

    def showScoreList(self):
        self.readScoreFile()

    def saveScore(self, name, score):
        self.writeToFile(name, score)

    def writeToFile(self, name, points):
        fileScore = open('scoreList.txt', 'a')
        fileScore.write(name)
        fileScore.write('\n')
        fileScore.write(points)
        fileScore.write('\n')
        fileScore.close()

    def readScoreFile(self):
        fileScore = open('scoreList.txt', 'r')
        for line in fileScore:
            self.tempScoreList.append(line.rstrip())
        fileScore.close()
        
        self.gamesPlayed = len(self.tempScoreList)/2

        for i in range(0, len(self.tempScoreList)):
            if len(self.tempScoreList) > 0:
                self.tempScoreList.pop()

    def getAmountGames(self):
        return self.gamesPlayed

    def sortScores(self):
        fileScore = open('scoreList.txt', 'r')
        for line in fileScore:
            self.tempScoreList.append(line.rstrip())
        fileScore.close()

        tempScoresOnly = []

        #sort and set topFice - score/name
        #
        for i in range(0, len(self.tempScoreList)):
            if i%2 == 1:
                tempScoresOnly.append(int(self.tempScoreList[i]))

        tempSorted = list(tempScoresOnly)
        tempSorted.sort()

        for i in range(0, 5):
            self.topFiveScore[i] = tempSorted[-(i+1)]
            self.topFiveName[i] = self.tempScoreList[tempScoresOnly.index(tempSorted[-(i+1)])*2]
            
        #
        #sort and set topFice - score/name
        
        for i in range(0, len(self.tempScoreList)):
            if len(self.tempScoreList) > 0:
                self.tempScoreList.pop()
        

    def getTop(self, atIndex):
        self.sortScores()
        return self.topFiveName[atIndex], self.topFiveScore[atIndex]
