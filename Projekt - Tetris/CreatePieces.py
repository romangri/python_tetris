##################################################
#IB910C-S VT2013
#Spel projekt
#Roman Grintsevich
#
#
#Python version: 2.7.5
#Operativ system: Windows 7
#
#
#Class used to create figures and background image
#
##################################################

from PIL import Image

##########
#I piece #
##########
iPiece = Image.new("RGB", (80, 20), "white")

blueSquare = Image.open("Image/blueSquare.png")

for x in range(0, 4):
    iPiece.paste(blueSquare, (x*20, 0))

iPiece.save('Image/iPiece.png')

iPiece = iPiece.rotate(90)

iPiece.save('Image/rotiPiece.png')

##########
#O piece #
##########
oPiece = Image.new("RGB", (40, 40), "white")

greenSquare = Image.open("Image/greenSquare.png")

for x in range(0, 2):    
    oPiece.paste(greenSquare, (x*20, x*20))
oPiece.paste(greenSquare, (20, 0))
oPiece.paste(greenSquare, (0, 20))

oPiece.save('Image/oPiece.png')

##########
#J piece #
##########
jPiece = Image.new("RGB", (60, 40), "white")

greySquare = Image.open("Image/greySquare.png")

for x in range(0, 3):    
    jPiece.paste(greySquare, (x*20, 0))
jPiece.paste(greySquare, (40, 20))

jPiece = jPiece.convert("RGBA")

pixdata = jPiece.load()

for y in xrange(jPiece.size[1]):
    for x in xrange(jPiece.size[0]):
        if pixdata[x, y] == (255, 255, 255, 255):
            pixdata[x, y] = (255, 255, 255, 0)

jPiece.save('Image/jPiece.png', 'PNG')

##########
#L piece #
##########
lPiece = Image.new("RGB", (60, 40), "white")

purpleSquare = Image.open("Image/purpleSquare.png")

for x in range(0, 3):    
    lPiece.paste(purpleSquare, (x*20, 0))
lPiece.paste(purpleSquare, (0, 20))

lPiece = lPiece.convert("RGBA")

pixdata = lPiece.load()

for y in xrange(lPiece.size[1]):
    for x in xrange(lPiece.size[0]):
        if pixdata[x, y] == (255, 255, 255, 255):
            pixdata[x, y] = (255, 255, 255, 0)

lPiece.save('Image/lPiece.png')


##########
#T piece #
##########
tPiece = Image.new("RGB", (60, 40), "white")

yellowSquare = Image.open("Image/yellowSquare.png")

for x in range(0, 3):    
    tPiece.paste(yellowSquare, (x*20, 0))
tPiece.paste(yellowSquare, (20, 20))

tPiece = tPiece.convert("RGBA")

pixdata = tPiece.load()

for y in xrange(tPiece.size[1]):
    for x in xrange(tPiece.size[0]):
        if pixdata[x, y] == (255, 255, 255, 255):
            pixdata[x, y] = (255, 255, 255, 0)

tPiece.save('Image/tPiece.png')

##########
#Z piece #
##########
zPiece = Image.new("RGB", (60, 40), "white")

redSquare = Image.open("Image/redSquare.png")

for x in range(0, 2):    
    zPiece.paste(redSquare, (x*20, 0))
    zPiece.paste(redSquare, ((x+1)*20, 20))

zPiece = zPiece.convert("RGBA")

pixdata = zPiece.load()

for y in xrange(zPiece.size[1]):
    for x in xrange(zPiece.size[0]):
        if pixdata[x, y] == (255, 255, 255, 255):
            pixdata[x, y] = (255, 255, 255, 0)

zPiece.save('Image/zPiece.png')

##########
#S piece #
##########
sPiece = Image.new("RGB", (60, 40), "white")

cyanSquare = Image.open("Image/cyanSquare.png")

for x in range(0, 2):    
    sPiece.paste(cyanSquare, ((x+1)*20, 0))
    sPiece.paste(cyanSquare, (x*20, 20))

sPiece = sPiece.convert("RGBA")

pixdata = sPiece.load()

for y in xrange(sPiece.size[1]):
    for x in xrange(sPiece.size[0]):
        if pixdata[x, y] == (255, 255, 255, 255):
            pixdata[x, y] = (255, 255, 255, 0)

sPiece.save('Image/sPiece.png')

##################
#Menu background #
##################

background = Image.new("RGB", (320, 640), (0,0,0))

# iPiece
# oPiece
# jPiece
# lPiece
# tPiece
# zPiece
# sPiece

for x in xrange(320):
    for y in xrange(640):
        #background.putpixel((x, y), (0, 255, 0))
        background.putpixel((x, y), (x/3, (x+y)/6, y/2))

background.paste(tPiece, (0, 540))

background.paste(iPiece, (0, 560))

background.paste(iPiece, (40, 560))

background.paste(oPiece, (60, 600))

#--#
background.paste(cyanSquare, (100, 620))
background.paste(cyanSquare, (120, 620))
background.paste(cyanSquare, (120, 600))
background.paste(cyanSquare, (140, 600))
#--#

#--#
background.paste(yellowSquare, (140, 620))
background.paste(yellowSquare, (160, 620))
background.paste(yellowSquare, (180, 620))
background.paste(yellowSquare, (160, 600))
#--#

#--#
background.paste(redSquare, (200, 620))
background.paste(redSquare, (200, 600))
background.paste(redSquare, (180, 600))
background.paste(redSquare, (220, 620))
#--#

#--#
background.paste(redSquare, (200, 620))
background.paste(redSquare, (200, 600))
background.paste(redSquare, (180, 600))
background.paste(redSquare, (220, 620))
#--#

#--#
background.paste(greySquare, (60, 580))
background.paste(greySquare, (80, 580))
background.paste(greySquare, (100, 580))
background.paste(greySquare, (100, 600))
#--#

#--#
background.paste(purpleSquare, (40, 380))
background.paste(purpleSquare, (60, 380))
background.paste(purpleSquare, (60, 400))
background.paste(purpleSquare, (60, 420))
#--#

background = background.convert("RGBA")

background.save('Image/background.png')

