##################################################
#IB910C-S VT2013
#Spel projekt
#Roman Grintsevich
#
#
#Python version: 2.7.5
#Operativ system: Windows 7
#
#J figure class
#position and rotation function
#
##################################################

class jFigure(object):
    def __init__(self):
        self.arrayX = [0, 0, 0, 0]
        self.arrayY = [0, 0, 0, 0]

        self.mode = 0 #0 start mode

        self.startPosX = [120, 140, 160, 160]
        self.startPosY = [40, 40, 40, 60]
        
    def rotateCords(self):
        #rotate 4
        self.mode += 1
        if self.mode == 4:
            self.mode = 0
        if self.mode == 0:
            #self.arrayX[0] = self.arrayX[0]
            #self.arrayX[1] = self.arrayX[1]
            self.arrayX[2] += 20
            self.arrayX[3] += 20

            self.arrayY[0] -= 40
            #self.arrayY[1] = self.arrayY[1]
            self.arrayY[2] -= 20
            self.arrayY[3] -= 20
            
        if self.mode == 1:
            #self.arrayX[0] = self.arrayX[0]
            #self.arrayX[1] = self.arrayX[1]
            self.arrayX[2] -= 40
            self.arrayX[3] -= 40

            #self.arrayY[0] = self.arrayY[0]
            #self.arrayY[1] = self.arrayY[1]
            self.arrayY[2] += 20
            self.arrayY[3] += 20

        if self.mode == 2:
            #self.arrayX[0] = self.arrayX[0]
            self.arrayX[1] -= 20
            self.arrayX[2] += 20
            self.arrayX[3] += 40

            #self.arrayY[0] = self.arrayY[0]
            self.arrayY[1] += 20
            #self.arrayY[2] = self.arrayY[2]
            self.arrayY[3] -= 20

        if self.mode == 3:
            #self.arrayX[0] = self.arrayX[0]
            self.arrayX[1] += 20
            #self.arrayX[2] = self.arrayX[2]
            self.arrayX[3] -= 20

            self.arrayY[0] += 40
            self.arrayY[1] -= 20
            #self.arrayY[2] = self.arrayY[2]
            self.arrayY[3] += 20

    def setArrayX(self, array):
        self.arrayX = array

    def setArrayY(self, array):
        self.arrayY = array

    def getStartX(self, i):
        return self.startPosX[i]

    def getStartY(self, i):
        return self.startPosY[i]

    def getMode(self):
        return self.mode

    def resetMode(self):
        self.mode = 0

    def getArrayX(self):
        return self.arrayX

    def getArrayY(self):
        return self.arrayY
