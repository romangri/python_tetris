##################################################
#IB910C-S VT2013
#Spel projekt
#Roman Grintsevich
#
#
#Python version: 2.7.5
#Operativ system: Windows 7
#
#O figure class
#position and rotation function
#
##################################################

from Positions import Position

class oFigure(object):
    def __init__(self):
        self.arrayX = [0, 0, 0, 0]
        self.arrayY = [0, 0, 0, 0]

        #start pos
        self.startPosX = [120, 140, 120, 140]
        self.startPosY = [40, 40, 60, 60]
        #start pos
        
        self.newPosition = Position()

        self.mode = 0
        
    def rotateCords(self):
        #no rotation needed
        print "rotate"

    def setArrayX(self, array):
        self.arrayX = array

    def setArrayY(self, array):
        self.arrayY = array

    def getStartX(self, i):
        return self.startPosX[i]

    def getStartY(self, i):
        return self.startPosY[i]

    def getMode(self):
        return 0

    def resetMode(self):
        self.mode = 0

    def getArrayX(self):
        return self.arrayX

    def getArrayY(self):
        return self.arrayY
