##################################################
#IB910C-S VT2013
#Spel projekt
#Roman Grintsevich
#
#
#Python version: 2.7.5
#Operativ system: Windows 7
#
#Main class
#
##################################################

import pygame, string
import sys, time
from pygame.locals import *
import random

#classes import
from lineFigure import iFigure
from squareFigure import oFigure
from jayFigure import jFigure
from loFigure import lFigure
from taFigure import tFigure
from zoFigure import zFigure
from soFigure import sFigure
from Fields import Field
from Scores import Score

from Positions import Position #pos class
from Colors import Color   #color class
from Buttons import Button #button class

#############
#Random value
#Function to
#random int
#############
class RandomValue:
    def __init__(self):
        value = 0

    def getRandomValue(self):
        value = random.randint(0, 6)
        return value
    

###########
#Game class
###########
class Game:
    def __init__(self):
        pygame.init()   #Init pygame
        self.newColor = Color()
        
        self.FPS = 30        #FPS

        self.run = True      #Game run

        self.newPosition = Position()
        self.newRandom = RandomValue()  #for random figure
        self.newScore = Score()

        #figure classes
        self.newIfigure = iFigure()
        self.newOfigure = oFigure()
        self.newJfigure = jFigure()
        self.newLfigure = lFigure()
        self.newTfigure = tFigure()
        self.newZfigure = zFigure()
        self.newSfigure = sFigure()

        self.pieceAmount = 0

        #field
        self.newField = Field()

        self.lowestY = [620,620,620,620]

        #figure class list
        self.figureList = [self.newIfigure, self.newOfigure, self.newJfigure, self.newLfigure, self.newTfigure, self.newZfigure, self.newSfigure]

        self.setDisplay = pygame.display.set_mode((320,640)) #Window
        pygame.display.set_caption('Python project - Tetris') #Window titel
        self.setDisplay.fill(self.newColor.getColor('white'))        #Windows background

        self.currentX = [0, 0, 0, 0]
        self.currentY = [0, 0, 0, 0]
        self.startX = 120
        self.startY = 40

        self.startPosXtutorial = 120 #start position tutorial
        self.startPosYtutorial = 70  #start position tutorial
        self.leftArrowFlag = True    #flag if left arrow pushed in tutorial
        self.rightArrowFlag = False  #flag if right arrow pushed in tutorial
        self.spaceButtonFlag = False #flag if space arrow pushed in tutorial
        self.leftCheck = False       #flag if left move done
        self.rightCheck = False      #flag if right move done
        self.spaceCheck = False      #flag if space move done
        self.spaceCounter = 0        #counter for space klicked

        self.currentFigure = 0       #current figure
        self.nextFigure = 0
        self.getFigure = True        #flag to get new random figure

        self.DOWN = 'down'
        self.RIGHT = 'right'
        self.LEFT = 'left'
        self.direction = 'NULL'

        self.moveLeft = 0   #move left
        self.moveRight = 0  #move right
        self.moveDown = 0   #move down (faster)
        self.rotateFlag = False     #totate

        self.speed = 1 #fall speed
        self.accelerate = 1 #speed up fall

        self.pause = False #pause var

        self.inMenu = True #menu

        self.nameFlag = False   #top score
        self.scoreFlag = False  #top score

        self.scoreListFlag = False #score

        self.optionsFlag = False    #options

        self.tutorialFlag = False   #tutorial

        self.scoreQfont = pygame.font.SysFont("Courier", 25)
        self.scoreQ = self.scoreQfont.render("Save score?", True, (255, 255, 255))
        self.enterNameStr = self.scoreQfont.render('Enter your name', True, (255, 240, 220))
        self.gamePause1 = self.scoreQfont.render("Game paused.", True, (255, 255, 255))
        self.gamePause2 = self.scoreQfont.render("Press F2 to resume.", True, (255, 255, 255))

        self.leftArrow = self.scoreQfont.render("Push left arrow", True, (255, 255, 255))
        self.rightArrow = self.scoreQfont.render("Push right arrow", True, (255, 255, 255))
        self.spaceButton = self.scoreQfont.render("Push space button", True, (255, 255, 255))
        self.downArrow1 = self.scoreQfont.render("Push down arrow to", True, (255, 255, 255))
        self.downArrow2 = self.scoreQfont.render("accelerate the fall.", True, (255, 255, 255))
        self.tutorialPause = self.scoreQfont.render("F2 - pause/resume.", True, (255, 255, 255))

        self.playerName = []
        
        ##########
        #load here

        #loading images
        self.iPiece = pygame.image.load('Image/iPiece.png')
        self.oPiece = pygame.image.load('Image/oPiece.png')
        self.jPiece = pygame.image.load('Image/jPiece.png')
        self.lPiece = pygame.image.load('Image/lPiece.png')
        self.tPiece = pygame.image.load('Image/tPiece.png')
        self.zPiece = pygame.image.load('Image/zPiece.png')
        self.sPiece = pygame.image.load('Image/sPiece.png')
        self.whiteSquare = pygame.image.load('Image/whiteSquare.png')
        self.background = pygame.image.load('Image/background.png')
        
        self.pieces = [self.iPiece, self.oPiece, self.jPiece, self.lPiece, self.tPiece, self.zPiece, self.sPiece]

        #sound
        #

        #loading sounds
        self.moveDone = pygame.mixer.Sound('Sound/moveDone.wav')
        self.rowCleared = pygame.mixer.Sound('Sound/rowCleared.wav')
        self.gameOver = pygame.mixer.Sound('Sound/gameOver.wav')
        self.gameStartedSound = pygame.mixer.Sound('Sound/newGame.wav')
        #
        #sound

        #menu
        #

        #creating buttons
        #main menu
        self.newButtonStart = Button()
        self.newButtonOption = Button()
        self.newButtonScore = Button()
        self.newButtonTutorial = Button()
        self.newButtonExit = Button()
        self.newButtonScoreMenu = Button()

        #options menu
        self.newButtonSpeedOne = Button()
        self.newButtonSpeedTwo = Button()
        self.newButtonSpeedThree = Button()
        self.newButtonOptionsMenu = Button()

        self.newButtonTutorialsMenu = Button()
        #
        #menu

        #score menu
        #
        #save score menu
        self.newButtonYes = Button()
        self.newButtonNo = Button()
        #
        #score menu
        
        #Random figure - current and next
        #
        
        #load here
        ##########

    #Run game   
    def runGame(self):
        
        self.fpsTime = pygame.time.Clock()
  
        while True:
            #update display background
            #
            self.setDisplay.blit(self.background, (0,0))
            #
            #update display background
                

            #start/new game
                     
            while self.inMenu:
                self.setDisplay.blit(self.background, (0,0))
                
                #                           surface,    x,  y,  width, height
                self.newButtonStart.createButton(self.setDisplay, 90, 120, 140, 50, 'Start')
                self.newButtonScore.createButton(self.setDisplay, 90, 180, 140, 50, 'Score')
                self.newButtonOption.createButton(self.setDisplay, 90, 240, 140, 50, 'Option')
                self.newButtonTutorial.createButton(self.setDisplay, 90, 300, 140, 50, 'Tutorial')
                self.newButtonExit.createButton(self.setDisplay, 90, 360, 140, 50, 'Exit')

                pygame.display.update()
                
                for event in pygame.event.get():
                    if event.type == pygame.QUIT:
                        pygame.quit()
                        sys.exit()
                    elif event.type == MOUSEBUTTONDOWN:
                        if self.newButtonStart.buttonPressed(pygame.mouse.get_pos()):
                            self.run = True
                            self.inMenu = False
                        elif self.newButtonScore.buttonPressed(pygame.mouse.get_pos()):
                            self.scoreListFlag = True
                            self.newScore.sortScores()
                        elif self.newButtonOption.buttonPressed(pygame.mouse.get_pos()):
                            self.optionsFlag = True
                        elif self.newButtonTutorial.buttonPressed(pygame.mouse.get_pos()):
                            self.tutorialFlag = True
                        elif self.newButtonExit.buttonPressed(pygame.mouse.get_pos()):
                            pygame.quit()
                            sys.exit()
                
                
            #
            #start/new game

                #top 5 score list
                #   
                while self.scoreListFlag:
                    self.setDisplay.fill(self.newColor.getColor('black'))
                    self.newButtonScoreMenu.createButton(self.setDisplay, 90, 500, 140, 50, 'Menu  ')

                    for event in pygame.event.get():
                        if event.type == pygame.QUIT:
                            pygame.quit()
                            sys.exit()
                        elif event.type == MOUSEBUTTONDOWN:
                            if self.newButtonScoreMenu.buttonPressed(pygame.mouse.get_pos()):
                                self.scoreListFlag = False

                    #get score list
                    #
                    topTitel = self.scoreQfont.render('Top five scores', True, (250, 250, 250))
                    self.setDisplay.blit(topTitel, (35, 50))

                    top5Name = self.scoreQfont.render('| Name', True, (150, 150, 150))
                    self.setDisplay.blit(top5Name, (15, 90))

                    top5Scores = self.scoreQfont.render('| Score', True, (150, 150, 150))
                    self.setDisplay.blit(top5Scores, (200, 90))

                    underScore = self.scoreQfont.render('___________________', True, (150, 150, 150))
                    self.setDisplay.blit(underScore, (15, 100))
                    
                    #+25 pixles each row
                    for i in range(0, 5):
                        topName = self.scoreQfont.render("| " + self.newScore.getTop(i)[0], True, (200, 200, 200))
                        self.setDisplay.blit(topName, (15, 135+(i*25)))

                        topScore = self.scoreQfont.render("| " + str(self.newScore.getTop(i)[1]), True, (200, 200, 200))
                        self.setDisplay.blit(topScore, (200, 135+(i*25)))
                    #
                    #get score list

                    pygame.display.update()
                    #
                    #top 5 score list


                while self.optionsFlag:
                    self.setDisplay.blit(self.background, (0,0))

                    speedToDisplay = 1

                    if self.speed == 1:
                        speedToDisplay = 1
                    elif self.speed == 5:
                        speedToDisplay = 2
                    elif self.speed == 10:
                        speedToDisplay = 3

                    self.currentSpeed = self.scoreQfont.render("Current speed: " + str(speedToDisplay), True, (255, 255, 255))
                    self.setDisplay.blit(self.currentSpeed, (50, 70))
        
                    self.newButtonSpeedOne.createButton(self.setDisplay, 90, 120, 140, 50, 'Speed 1')
                    self.newButtonSpeedTwo.createButton(self.setDisplay, 90, 180, 140, 50, 'Speed 2')
                    self.newButtonSpeedThree.createButton(self.setDisplay, 90, 240, 140, 50, 'Speed 3')
                    self.newButtonOptionsMenu.createButton(self.setDisplay, 90, 300, 140, 50, 'Menu  ')                    

                    for event in pygame.event.get():
                        if event.type == pygame.QUIT:
                            pygame.quit()
                            sys.exit()
                        elif event.type == MOUSEBUTTONDOWN:
                            if self.newButtonSpeedOne.buttonPressed(pygame.mouse.get_pos()):
                                self.speed = 1
                            elif self.newButtonSpeedTwo.buttonPressed(pygame.mouse.get_pos()):
                                self.speed = 5
                            elif self.newButtonSpeedThree.buttonPressed(pygame.mouse.get_pos()):
                                self.speed = 10
                            elif self.newButtonOptionsMenu.buttonPressed(pygame.mouse.get_pos()):
                                self.optionsFlag = False

                    pygame.display.update()

                while self.tutorialFlag:
                    self.setDisplay.fill(self.newColor.getColor('black'))
                    self.newButtonTutorialsMenu.createButton(self.setDisplay, 90, 550, 140, 50, 'Menu  ')
                    
                    for event in pygame.event.get():
                        if event.type == pygame.QUIT:
                            pygame.quit()
                            sys.exit()
                        if event.type == MOUSEBUTTONDOWN:
                            if self.newButtonTutorialsMenu.buttonPressed(pygame.mouse.get_pos()):
                                self.tutorialFlag = False                                

                    #tutorial
                    #
                    #pressed key
                        keys = pygame.key.get_pressed()

                        if keys[pygame.K_LEFT]:
                            if self.startPosXtutorial > 0 and self.leftArrowFlag:
                                self.startPosXtutorial -= 20
                        
                        if keys[pygame.K_RIGHT] and self.rightArrowFlag:
                            if self.startPosXtutorial < 260:
                                self.startPosXtutorial += 20
                                
                        if keys[pygame.K_DOWN]:
                            print "down"

                        if keys[pygame.K_SPACE]:
                            if self.spaceButtonFlag:
                                self.spaceCounter += 1
                                self.pieces[4] = pygame.transform.rotate(self.pieces[4], 90)

                    if self.startPosXtutorial == 0:
                        self.leftArrowFlag = False
                        self.rightArrowFlag = True
                        self.spaceButtonFlag = False
                        self.leftCheck = True
                        
                    if self.startPosXtutorial == 260:
                        self.leftArrowFlag = True
                        self.rightArrowFlag = False
                        self.spaceButtonFlag = False
                        self.rightCheck = True

                    if self.leftCheck and self.rightCheck:
                        self.leftArrowFlag = False
                        self.rightArrowFlag = False
                        self.spaceButtonFlag = True
                        self.startPosXtutorial = 120


                    if self.spaceCounter == 4:
                        self.spaceButtonFlag = False
                        self.setDisplay.blit(self.downArrow1, (20, 15))
                        self.setDisplay.blit(self.downArrow2, (20, 35))
                        self.setDisplay.blit(self.tutorialPause, (20, 100))

                    if self.leftArrowFlag:
                        self.setDisplay.blit(self.leftArrow, (50, 15))
                    if self.rightArrowFlag:
                        self.setDisplay.blit(self.rightArrow, (50, 15))
                    if self.spaceButtonFlag:
                        self.setDisplay.blit(self.spaceButton, (50, 15))
                        

                    if self.spaceCounter != 4:
                        self.setDisplay.blit(self.pieces[4], (self.startPosXtutorial,  self.startPosYtutorial))

                    
                    #
                    #tutorial
                    
                    pygame.display.update()
                    self.fpsTime.tick(self.FPS)

            self.gameStartedSound.play()    
            while self.run:
                self.setDisplay.fill(self.newColor.getColor('black'))

                #game over
                #
                if self.newField.gameOverFlag():
                    self.run = False
                    self.inMenu = True
                    self.newField.clearAll()
                    self.gameOver.play()

                    #save score?
                    #

                    #get player name
                    #

                    self.scoreFlag = True
                    
                    while self.scoreFlag:
                        self.setDisplay.fill(self.newColor.getColor('black'))

                        #text "Save score?"
                        #buttons Yes / No
                        self.setDisplay.blit(self.scoreQ, (80, 140))
                        
                        self.newButtonYes.createButton(self.setDisplay, 90, 200, 140, 50, 'Yes')
                        self.newButtonNo.createButton(self.setDisplay, 90, 260, 140, 50, 'No')

                        keyName = ''
                        formatedName = ''
                        yesButtonFlag = False
                        #erased = False
            
                        for event in pygame.event.get():
                            if event.type == pygame.QUIT:
                                pygame.quit()
                                sys.exit()
                            elif event.type == MOUSEBUTTONDOWN:
                                if self.newButtonYes.buttonPressed(pygame.mouse.get_pos()):
                                    self.scoreFlag = False
                                    #type in name
                                    #
                                    yesButtonFlag = True
                                    self.setDisplay.blit(self.enterNameStr, (45, 320))
                                    
                                    while yesButtonFlag:
                                        self.setDisplay.fill(self.newColor.getColor('black'))
                                        
                                        for eventName in pygame.event.get():
                                            if event.type == pygame.QUIT:
                                                pygame.quit()
                                                sys.exit()
                                            elif eventName.type == KEYDOWN:
                                                if eventName.key != K_BACKSPACE:
                                                    keyName = pygame.key.name(eventName.key)
                                                    self.playerName.append(keyName)
                                                    formatedName = string.join(self.playerName, "")
                                                
                                                if eventName.key == K_RETURN:
                                                    self.newScore.saveScore(formatedName[:-6], str(self.newField.getRowsDone()))
                                                    yesButtonFlag = False
                                                elif eventName.key == K_BACKSPACE:
                                                    formatedName = formatedName[:-1]
                                                    if len(self.playerName) > 0:
                                                        self.playerName.pop()
                                                           
                                        nameToDisplay = self.scoreQfont.render('Name:' + formatedName, True, (200, 200, 200))
                                        info1 = self.scoreQfont.render('Type your name!', True, (150, 150, 150))
                                        info2 = self.scoreQfont.render('Press enter when done.', True, (150, 150, 150))
                                        self.setDisplay.blit(info1, (45, 200))
                                        self.setDisplay.blit(info2, (2, 250))
                                        self.setDisplay.blit(nameToDisplay, (45, 400))     
                                        pygame.display.update()
                                    #
                                    #type in name
                                    break
                                elif self.newButtonNo.buttonPressed(pygame.mouse.get_pos()):
                                    self.scoreFlag = False
                                    break

                            

                        
                        pygame.display.update()
                    #
                    #get player name
                    
                    #
                    #save score?
                    
                    break
                #
                #game over
                
                for event in pygame.event.get():
                    if event.type == QUIT:
                        pygame.quit()
                        sys.exit()

                    #pressed key
                    keys = pygame.key.get_pressed()

                    if keys[pygame.K_LEFT]:
                        if self.newPosition.getLeftX(self.currentX) > 0:
                            self.direction = self.LEFT
                            self.moveLeft = -20
                    
                        
                    if keys[pygame.K_RIGHT]:
                        if self.newPosition.getRightX(self.currentX) < 300:
                            self.direction = self.RIGHT
                            self.moveRight = 20
                    if keys[pygame.K_DOWN]:
                        if self.speed == 1:
                            self.accelerate = 20
                        elif self.speed == 5:
                            self.accelerate = 4
                        elif self.speed == 10:
                            self.accelerate = 2

                    if keys[pygame.K_SPACE]:
                        self.rotateFlag = True

                    #pause game
                    if keys[pygame.K_F2]:
                        self.pause = True
                        self.setDisplay.blit(self.gamePause1, (80, 50))
                        self.setDisplay.blit(self.gamePause2, (20, 100))
                        pygame.display.update()
                        while self.pause:
                            for event in pygame.event.get():
                                if event.type == pygame.KEYDOWN:
                                    if event.key == pygame.K_F2:
                                        self.pause = False
                    
                    #pressed key

                self.moveDown = 1

                ##########################
                #get random current figure
                if self.getFigure:

                    self.currentFigure = self.newRandom.getRandomValue()
                    self.nextFigure = self.newRandom.getRandomValue()

                    if self.currentFigure == 0:
                        for i in range(0, 4):
                            self.currentX[i] = self.figureList[self.currentFigure].getStartX(i)
                            self.currentY[i] = self.figureList[self.currentFigure].getStartY(i)
                        #get mode
                        if self.figureList[self.currentFigure].getMode() == 1:
                            self.figureList[self.currentFigure].resetMode()
                            self.pieces[self.currentFigure] = pygame.transform.rotate(self.pieces[self.currentFigure], 90)
                                                
                    elif self.currentFigure == 1:
                        if self.figureList[self.currentFigure].getMode() != 0:
                            #repaint
                            
                            #repaint
                            self.figureList[self.currentFigure].resetMode()
                        
                        for i in range(0, 4):
                            self.currentX[i] = self.figureList[self.currentFigure].getStartX(i)
                            self.currentY[i] = self.figureList[self.currentFigure].getStartY(i)
                        
                    elif self.currentFigure == 2:
                        #pos
                        for i in range(0, 4):
                            self.currentX[i] = self.figureList[self.currentFigure].getStartX(i)
                            self.currentY[i] = self.figureList[self.currentFigure].getStartY(i)

                        #get mode and reset at start if not 0 mode
                        if self.figureList[self.currentFigure].getMode() != 0:
                            if self.figureList[self.currentFigure].getMode() == 1:
                                #rotate
                                self.figureList[self.currentFigure].resetMode()
                                self.pieces[self.currentFigure] = pygame.transform.rotate(self.pieces[self.currentFigure], 270)

                            if self.figureList[self.currentFigure].getMode() == 2:
                                #rotate
                                self.figureList[self.currentFigure].resetMode()
                                self.pieces[self.currentFigure] = pygame.transform.rotate(self.pieces[self.currentFigure], 180)

                            if self.figureList[self.currentFigure].getMode() == 3:
                                #rotate
                                self.figureList[self.currentFigure].resetMode()
                                self.pieces[self.currentFigure] = pygame.transform.rotate(self.pieces[self.currentFigure], 90)
                                                        
                    elif self.currentFigure == 3:
                        for i in range(0, 4):
                            self.currentX[i] = self.figureList[self.currentFigure].getStartX(i)
                            self.currentY[i] = self.figureList[self.currentFigure].getStartY(i)
                            
                        #get mode and reset at start if not 0 mode
                        if self.figureList[self.currentFigure].getMode() != 0:
                            if self.figureList[self.currentFigure].getMode() == 1:
                                #rotate
                                self.figureList[self.currentFigure].resetMode()
                                self.pieces[self.currentFigure] = pygame.transform.rotate(self.pieces[self.currentFigure], 270)

                            if self.figureList[self.currentFigure].getMode() == 2:
                                #rotate
                                self.figureList[self.currentFigure].resetMode()
                                self.pieces[self.currentFigure] = pygame.transform.rotate(self.pieces[self.currentFigure], 180)

                            if self.figureList[self.currentFigure].getMode() == 3:
                                #rotate
                                self.figureList[self.currentFigure].resetMode()
                                self.pieces[self.currentFigure] = pygame.transform.rotate(self.pieces[self.currentFigure], 90) 
                            
                    elif self.currentFigure == 4:
                        for i in range(0, 4):
                            self.currentX[i] = self.figureList[self.currentFigure].getStartX(i)
                            self.currentY[i] = self.figureList[self.currentFigure].getStartY(i)

                        #get mode and reset at start if not 0 mode
                        if self.figureList[self.currentFigure].getMode() != 0:
                            if self.figureList[self.currentFigure].getMode() == 1:
                                #rotate
                                self.figureList[self.currentFigure].resetMode()
                                self.pieces[self.currentFigure] = pygame.transform.rotate(self.pieces[self.currentFigure], 270)

                            if self.figureList[self.currentFigure].getMode() == 2:
                                #rotate
                                self.figureList[self.currentFigure].resetMode()
                                self.pieces[self.currentFigure] = pygame.transform.rotate(self.pieces[self.currentFigure], 180)

                            if self.figureList[self.currentFigure].getMode() == 3:
                                #rotate
                                self.figureList[self.currentFigure].resetMode()
                                self.pieces[self.currentFigure] = pygame.transform.rotate(self.pieces[self.currentFigure], 90)
                          
                    elif self.currentFigure == 5:
                        for i in range(0, 4):
                            self.currentX[i] = self.figureList[self.currentFigure].getStartX(i)
                            self.currentY[i] = self.figureList[self.currentFigure].getStartY(i)

                        #get mode
                        if self.figureList[self.currentFigure].getMode() == 1:
                            self.figureList[self.currentFigure].resetMode()
                            self.pieces[self.currentFigure] = pygame.transform.rotate(self.pieces[self.currentFigure], 90)
                        
                    elif self.currentFigure == 6:
                        for i in range(0, 4):
                            self.currentX[i] = self.figureList[self.currentFigure].getStartX(i)
                            self.currentY[i] = self.figureList[self.currentFigure].getStartY(i)

                        #get mode
                        if self.figureList[self.currentFigure].getMode() == 1:
                            self.figureList[self.currentFigure].resetMode()
                            self.pieces[self.currentFigure] = pygame.transform.rotate(self.pieces[self.currentFigure], 90)

                        self.currentY[0] -= 20
        
                    self.getFigure = False
                    
                ###########
                #draw here

                ###########
                #change pos

                if self.direction == self.LEFT:
                    for i in range(0, 4):
                        self.currentX[i] += self.moveLeft;
                elif self.direction == self.RIGHT:
                    for i in range(0, 4):
                        self.currentX[i] += self.moveRight;
                elif self.direction == self.DOWN:
                    print "direcation down"

                #reset move variable
                self.moveLeft = 0
                self.moveRight = 0

                #rotate
                if self.rotateFlag:
                    #update pos in class
                    self.figureList[self.currentFigure].setArrayX(self.currentX)
                    self.figureList[self.currentFigure].setArrayY(self.currentY)
                    #update pos in class
                    
                    #rotate picrute -ok-
                    self.pieces[self.currentFigure] = pygame.transform.rotate(self.pieces[self.currentFigure], 90)
                    #call klass rotate (pos update)
                    self.figureList[self.currentFigure].rotateCords()

                    

                    #J figure corr
                    if self.currentFigure == 2:
                        if self.figureList[self.currentFigure].getMode() == 3:
                            self.currentY[0] -= 40
                        if self.figureList[self.currentFigure].getMode() == 0:
                            self.currentY[0] += 40
                    #L figure corr
                    if self.currentFigure == 3:
                        if self.figureList[self.currentFigure].getMode() == 2:
                            self.currentY[0] -= 20
                        if self.figureList[self.currentFigure].getMode() == 3:
                            self.currentY[0] += 20
                    #T figure corr
                    if self.currentFigure == 4:
                        if self.figureList[self.currentFigure].getMode() == 2:
                            self.currentY[0] -= 20
                        if self.figureList[self.currentFigure].getMode() == 0:
                            self.currentY[0] += 20
                    #Z figure corr
                    if self.currentFigure == 5:
                        if self.figureList[self.currentFigure].getMode() == 1:
                            self.currentY[0] -= 20
                        if self.figureList[self.currentFigure].getMode() == 0:
                            self.currentY[0] += 20
                    #S figure corr
                    if self.currentFigure == 6:
                        if self.figureList[self.currentFigure].getMode() == 1:
                            self.currentY[0] += 20
                        if self.figureList[self.currentFigure].getMode() == 0:
                            self.currentY[0] -= 20

                    self.currentX = self.figureList[self.currentFigure].getArrayX()
                    self.currentY = self.figureList[self.currentFigure].getArrayY()
                    
                #reset rotate flag                 
                    self.rotateFlag = False

                #update pos in class
                self.figureList[self.currentFigure].setArrayX(self.currentX)
                self.figureList[self.currentFigure].setArrayY(self.currentY)
                #update pos in class

                #check lowest available y
                #
                for i in range(0, 4):
                    if(self.newField.getLowestY(self.currentX[i]/20) != 0):
                        self.lowestY[i] = self.newField.getLowestY(self.currentX[i]/20)*20
                        self.lowestY[i] -= 20
                #
                #check lowest available y

                #fall version 2
                #
                if self.newField.getNextY(self.currentX[0],self.currentY[0]) == 0 and self.newField.getNextY(self.currentX[1],self.currentY[1]) == 0 and self.newField.getNextY(self.currentX[2],self.currentY[2]) == 0 and self.newField.getNextY(self.currentX[3],self.currentY[3]) == 0:
                    for i in range(0, 4):
                        self.currentY[i] += self.moveDown*self.speed*self.accelerate
                else:
                    self.getFigure = True
                    self.moveDown = 0
                    #corr cords
                    #
                    #J piece
                    if self.currentFigure == 2:
                        if self.figureList[self.currentFigure].getMode() == 3:
                            self.currentY[0] += 40
                    #L piece
                    if self.currentFigure == 3:
                        if self.figureList[self.currentFigure].getMode() == 2:
                            self.currentY[0] += 20
                    #T piece
                    if self.currentFigure == 4:
                        if self.figureList[self.currentFigure].getMode() == 2:
                            self.currentY[0] += 20
                    if self.currentFigure == 4:
                        if self.figureList[self.currentFigure].getMode() == 3:
                            self.currentY[0] += 20
                    #Z piece
                    if self.currentFigure == 5:
                        if self.figureList[self.currentFigure].getMode() == 1:
                            self.currentY[0] += 20
                    #S piece
                    if self.currentFigure == 6:
                        if self.figureList[self.currentFigure].getMode() == 0:
                            self.currentY[0] += 20
                    #
                    #corr cords
                    
                    for i in range(0, 4):
                        self.newField.setBoard(self.currentX[i]/20, self.currentY[i]/20)
                    
                    #corr cords back
                    #
                    #J piece
                    if self.currentFigure == 2:
                        if self.figureList[self.currentFigure].getMode() == 3:
                            self.currentY[0] -= 40
                    #L piece
                    if self.currentFigure == 3:
                        if self.figureList[self.currentFigure].getMode() == 2:
                            self.currentY[0] -= 20
                    #T piece
                    if self.currentFigure == 4:
                        if self.figureList[self.currentFigure].getMode() == 2:
                            self.currentY[0] -= 20
                    if self.currentFigure == 4:
                        if self.figureList[self.currentFigure].getMode() == 3:
                            self.currentY[0] -= 20
                    #Z piece
                    if self.currentFigure == 5:
                        if self.figureList[self.currentFigure].getMode() == 1:
                            self.currentY[0] -= 20
                    #S piece
                    if self.currentFigure == 6:
                        if self.figureList[self.currentFigure].getMode() == 0:
                            self.currentY[0] -= 20
                    #
                    #corr cords back

                    #check rows
                    #
                    self.newField.checkRows()
                    if self.newField.checkRowFlag():
                        self.rowCleared.play()
                        self.newField.resetRowFlag()
                    #
                    #check rows

                    #sound
                    #
                    self.accelerate = 1
                    self.moveDone.play()
                    #
                    #sound
                    
                #
                #fall version 2

                #draw busy pos
                #
                for cols in range(0, 16):
                    for rows in range(0, 32):
                        if(self.newField.getUsedSquare(cols, rows) == 1):
                            self.setDisplay.blit(self.whiteSquare, (cols*20,  rows*20))
                #
                #draw busy pos

                #change caption
                #

                pygame.display.set_caption("Points: " + str(self.newField.getRowsDone()))

                #
                #change caption
                    
                #change pos
                ###########

                #send pos to figure class
                
                #send pos to figure class

                self.setDisplay.blit(self.pieces[self.currentFigure], (self.currentX[0],  self.currentY[0]))

                pygame.display.update()
                self.fpsTime.tick(self.FPS)
                #draw here
                ##########

###########
#Main class
###########

class Main:
    def __init__(self):
        newGame = Game()
        newGame.runGame()


###############
#Run main class
###############

newMain = Main()
