##################################################
#IB910C-S VT2013
#Spel projekt
#Roman Grintsevich
#
#
#Python version: 2.7.5
#Operativ system: Windows 7
#
#s figure class
#positions and rotation function
#
##################################################

class sFigure(object):
    def __init__(self):
        self.arrayX = [0, 0, 0, 0]
        self.arrayY = [0, 0, 0, 0]

        self.mode = 0 #0 start mode

        self.startPosX = [120, 140, 140, 160]
        self.startPosY = [60, 60, 40, 40]
        
    def rotateCords(self):
        #rotate 2
        self.mode += 1 
        #mode 1      
        if self.mode % 2 == 0:
            #self.arrayX[0] =
            self.arrayX[1] += 20
            #self.arrayX[2] = 
            self.arrayX[3] += 20

            self.arrayY[0] += 20
            #self.arrayY[1] = 
            self.arrayY[2] -= 20
            self.arrayY[3] -= 40

        #mode 2        
        if self.mode % 2 == 1:
            #self.arrayX[0] =
            self.arrayX[1] -= 20
            #self.arrayX[2] =
            self.arrayX[3] -= 20

            self.arrayY[0] -= 20
            #self.arrayY[1] = 
            self.arrayY[2] += 20
            self.arrayY[3] += 40

    def setArrayX(self, array):
        self.arrayX = array

    def setArrayY(self, array):
        self.arrayY = array

    def getStartX(self, i):
        return self.startPosX[i]

    def getStartY(self, i):
        return self.startPosY[i]

    def getMode(self):
        return self.mode % 2

    def resetMode(self):
        self.mode = 0

    def getArrayX(self):
        return self.arrayX

    def getArrayY(self):
        return self.arrayY
