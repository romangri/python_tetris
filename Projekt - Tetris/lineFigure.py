##################################################
#IB910C-S VT2013
#Spel projekt
#Roman Grintsevich
#
#
#Python version: 2.7.5
#Operativ system: Windows 7
#
#I figure class
#position and rotation function
#
##################################################

from Positions import Position

class iFigure(object):
    def __init__(self):
        self.arrayX = [0, 0, 0, 0]
        self.arrayY = [0, 0, 0, 0]

        self.mode = 0

        self.startPosX = [120, 140, 160, 180]
        self.startPosY = [40, 40, 40, 40]

        self.newPosition = Position()
       
    def rotateCords(self):    
        minX = self.newPosition.getLeftX(self.arrayX)
        minY = self.newPosition.getUpperY(self.arrayY)

        #mode 1      
        if self.mode % 2 == 0:
           for i in range(0, 4):
                self.arrayX[i] = minX
                self.arrayY[i] = (minY + (i*20))

        #mode 2        
        if self.mode % 2 == 1:
            for i in range(0, 4):
                self.arrayX[i] = (minX + (i*20))
                self.arrayY[i] = minY
                
        #reset mode        
        self.mode += 1

    def setArrayX(self, array):
        self.arrayX = array

    def setArrayY(self, array):
        self.arrayY = array

    def getStartX(self, i):
        return self.startPosX[i]

    def getStartY(self, i):
        return self.startPosY[i]

    def getMode(self):
        return self.mode % 2

    def resetMode(self):
        self.mode = 0

    def getArrayX(self):
        return self.arrayX

    def getArrayY(self):
        return self.arrayY
